#!/bin/bash
#
# Copyright 2017 fgalindo@talend.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
#
################################################################################
#
# SCP files to create accounts in docker repositories
#
################################################################################
#

#Global variables
apac_emea_us_user_pass_file=".apac-emea-us-user-pass"
follow_the_sun_user_pass_file=".follow-the-sun-user-pass"
username_password_files=("$apac_emea_us_user_pass_file" "$follow_the_sun_user_pass_file") # user password files
apac_repo_ip='35.198.198.143'
emea_repo_ip='35.195.241.134'
us_repo_ip='35.229.83.178'
apac_support_docker_registry_ip='35.186.155.93'
emea_support_docker_registry_ip='35.197.208.15'
us_support_docker_registry_ip='35.199.58.165'
follow_the_sun_support_docker_registry_ip='35.197.133.165'
repositories_ip=("$apac_repo_ip" "$emea_repo_ip" "$us_repo_ip" "$apac_support_docker_registry_ip" "$emea_support_docker_registry_ip" "$us_support_docker_registry_ip" "$follow_the_sun_support_docker_registry_ip")
create_accounts_script='create-accounts.sh'
remote_work_dir='/home/talend'
remote_user='talend'
project_ssh_keys=('gcp-customer-support' 'gcp-support-self-service')
ssh_keys_dir='~/.ssh'


# [MAIN start]
function main() {
  # edit "$create_accounts_script" before sending to remote
  if [[ -f "$create_accounts_script" ]]; then
    edit_create_accounts_script
  else
    echo "missing $create_accounts_script file"
    exit 0
  fi

  for repo_ip in "${repositories_ip[@]}"; do
    # echo $repo
    # echo "$create_accounts_script"
    # echo "${username_password_files[@]}"
    if [[ $repo_ip == $apac_repo_ip ]] || [[ $repo_ip == $emea_repo_ip ]] || [[ $repo_ip == $us_repo_ip ]]; then
      scp_to_repo "$repo_ip" "${project_ssh_keys[1]}"
    else
      scp_to_repo "$repo_ip" "${project_ssh_keys[0]}"
    fi

  done
}
# [END]



# [START edit_create_accounts_script]
function edit_create_accounts_script() {

  username_password_var="username_password_files=(${username_password_files[@]})"
  sed -i '' -e "s/.*username_password_files=.*/$username_password_var # user password files, added by scp-create-accounts.sh/g" $create_accounts_script
  sed -i '' -e "s~.*work_dir=.*~work_dir=$remote_work_dir # work directory, added by scp-create-accounts.sh~g" $create_accounts_script
}
# [END edit_create_accounts_script]



# [START scp_to_repo]
function scp_to_repo() {
  local repository_ip=$1
  local ssh_keys=$2

  if [[ $repository_ip == $follow_the_sun_support_docker_registry_ip ]]; then
    local user_pass_file=${username_password_files[1]}
  else
    local user_pass_file=${username_password_files[0]}
  fi

  echo ""
  echo "Connecting to repo: $repository_ip"
  echo "....................................................................."
  scp -i "$ssh_keys_dir/$ssh_keys" "$create_accounts_script" "$user_pass_file" "$remote_user@$repository_ip:$remote_work_dir"

  if [[ $? != 0 ]]; then
    echo "Unsuccessful SCP to $repository_ip:$remote_work_dir"

  else
    echo ""
    echo "Changing passwords for users @ registry $repository_ip:"
    echo "....................................................................."
    ssh_results=$( ssh -i "$ssh_keys_dir/$ssh_keys" "$remote_user@$repository_ip" <<HERE
              cd "$remote_work_dir"
              chmod +x "$create_accounts_script"
              ./"$create_accounts_script"
              rm -rf "$create_accounts_script" "$user_pass_file"
HERE )
    echo "....................................................................."
  fi
}
# [STOP scp_to_repo]


main "$@"
