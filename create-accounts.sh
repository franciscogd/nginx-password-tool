#!/bin/bash
#
# Copyright 2017 fgalindo@talend.com
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
#
################################################################################
#
# Create docker repositories accounts
#
################################################################################
#

#Global variables
username_password_files=(.apac-emea-us-user-pass .follow-the-sun-user-pass) # user password files, added by scp-create-accounts.sh
work_dir=/home/talend # work directory, added by scp-create-accounts.sh
follow_the_sun_repo_hostname='follow-the-sun-support-docker-registry'
nginx_dir='/docker-registry/nginx'

# [MAIN start]
function main() {
  read_files
  #rm -rf "${username_password_files[@]}"
}
#[END]



# [START read_files]
function read_files() {
  local vm_hostname=$(hostname)
  local username
  local password

  if [[ $vm_hostname == $follow_the_sun_repo_hostname ]]; then
    user_pass_file="${username_password_files[1]}"
  else
    user_pass_file="${username_password_files[0]}"
  fi

  while IFS='n\' read -r line || [[ -n ${line} ]] ; do
    username=$(echo ${line} | awk '{print $1}')
    password=$(echo ${line} | awk '{print $2}')
    create_accounts $username $password
  done < $user_pass_file
}
# [STOP read_files]



# [START create_accounts]
function create_accounts() {
  local username=$1
  local password=$2
  cd "$nginx_dir"
  sudo htpasswd -i -b registry.password "$username" "$password"
}
# [STOP create_accounts]



main "$@"
